﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightsProjectWeb.DTO
{
    public class AirlineCompanyDTO // destination
    {
        public long Id;
        public string CompanyName;
        public string CountryId;
        public string CountryName;

        /*
         * source
        public class AirlineCompany
        {
            public long Id;
            public string Name;
            public string Password;
            public int CountryId;

        }
        */
    }
}
