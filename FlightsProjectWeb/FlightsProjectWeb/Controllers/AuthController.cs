﻿using FlightsProjectWeb.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FlightsProjectWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpPost("token")]
        public async Task<ActionResult> GetToken([FromBody] UserDetailsDTO userDetails)
        {
            // 1) try login, with userDetails

            // await call FlightSystemCenter.Login(userDetails.Name, userDetails.Password);
            // 1 login failed
            //if (loginResult == false)
            //{
                //return Unauthorized("login failed");
            //}
            // 2 success 
            //   facade, LoginToken<T>, role 

            // 2) create key
            // security key
            string securityKey =
       "this_is_our_supper_long_security_key_for_token_validation_project_2018_09_07$smesk.in";

            // symmetric security key
            var symmetricSecurityKey = new
                SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            // signing credentials
            var signingCredentials = new
                  SigningCredentials(symmetricSecurityKey,
                  SecurityAlgorithms.HmacSha256Signature);

            // 3) create claim for specific role
            // add claims
            var claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Role, "Administrator")); // --> here use the role from the login result
            claims.Add(new Claim("userid", "1")); // --> here use the user_id from the result
            claims.Add(new Claim("username", "admin1")); // --> here use the name from the login result

            // 4) create token
            var token = new JwtSecurityToken(
            issuer: "smesk.in", // change to something better
            audience: "readers", // change to something better
            expires: DateTime.Now.AddHours(1), // should be configurable
            signingCredentials: signingCredentials,
            claims: claims);

            // 5) return token
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}
