﻿namespace ProjectPartA
{
    public class LoginToken<T> : ILoginToken where T : IUser
    {
        public T user;
    }
}