﻿namespace ProjectPartA
{
    public class Administrator : IUser
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}